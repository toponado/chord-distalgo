import os
class Parser():
    def GetConfig(self,filename):
        lines = [line.strip() for line in open(filename)]
        N = int(lines[0][2])
        keys = lines[1][5:].split(' ')
        keynum = []
        for x in keys:
            keynum.append(int(x))

        return N,keynum

def main():
    p = Parser()
    N,l = p.GetConfig("config.txt")
    print(N)
    print(l)

