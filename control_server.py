import os, time, sys, random
from parser_config import Parser

handlers = {}
usage_str = "command: i [server_id] [keynum]	  #ask one server to locate keynum\n" + \
            "command: ? [key1] [key2] .... [keyN] #ask arbitray servers to locate a sequence keys\n" + \
            "command: s [server_id] 		  #stop a server\n" + \
            "command: r [server_id] 		  #restart a server\n" + \
            "command: h 			  #help\n"

def control_server( ):
    N,keylist = Parser().GetConfig("config.txt")
    print(N)
    print(keylist)
    for x in keylist:
        name = "pipe_{0}".format(x)
        print(name)
        if not os.path.exists(name):
            os.mkfifo(name)  
        pipeout = os.open(name, os.O_WRONLY)
        handlers[x] = pipeout

    while True:
        s = input("please input command: \n")
        try:
            if s[0] == 'h':
                print(usage_str)
                print("server list: " + " ".join(str(x) for x in keylist))
                print("\n")
            elif s[0] == 'i':
                nums = s[2:].split(' ')
                pid = int(nums[0])
                lookupkey = int(nums[1])
                print("command {0}".format(pid))
                if pid not in handlers:
                    print("invalid pid".format(pid))
                    continue
                commandstr = "i {0}\n".format(lookupkey)
                os.write(handlers[pid], bytes(commandstr,"utf-8"))
            elif s[0] == '?':
                nums = s[2:].split(' ')
                lookupkeys = []
                for x in nums:
                    lookupkeys.append(int(x))
                if len(lookupkeys) > len(keylist):
                    lookupkeys = lookupkeys[0:len(keylist)]
                servers = random.sample(keylist,len(lookupkeys))
                print(servers)
                for i in range(0,len(servers)):
                    commandstr = "i {0}\n".format(lookupkeys[i])
                    print (commandstr)
                    os.write(handlers[servers[i]], bytes(commandstr,"utf-8"))
                    print(servers[i])

            elif s[0] == 's':
                nums = s[2:].split(' ')
                pid = int(nums[0])
                print("command stop server{0}".format(pid))
                if pid not in handlers:
                    print("invalid pid".format(pid))
                    continue
                commandstr = "stop\n"
                os.write(handlers[pid], bytes(commandstr,"utf-8"))
            elif s[0] == 'r': #restart one chord server
                nums = s[2:].split(' ')
                pid = int(nums[0])
                print("command restart server{0}".format(pid))
                if pid not in handlers:
                    print("invalid pid".format(pid))
                    continue
                commandstr = "start\n"
                os.write(handlers[pid], bytes(commandstr,"utf-8"))
            else:
                input("sample command: i 2 8")
        except:
                print("exception catch when parser the command")

def disply_path_server():
    name = "pipe_path"
    if not os.path.exists(name):
        os.mkfifo(name)
    pipein = open(name,'r')
    while True:
        line = pipein.readline()
        if line[0] == 'P':
            print(line[2:])
        else:
            print(line)

pid = os.fork()
if pid != 0:
    control_server()
else:
    disply_path_server()
