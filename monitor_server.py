import os, time, sys
from parser_config import Parser
import logging, logging.handlers
pipe_name = 'pipe_test'

x = logging.getLogger("monitor_log")
x.setLevel(logging.DEBUG)
f = logging.Formatter("%(levelname)s %(asctime)s %(message)s","%I:%M:%S")
h1 = logging.FileHandler("monitor.log")
h1.setFormatter(f)
h1.setLevel(logging.DEBUG)
h2 = logging.StreamHandler()
h2.setFormatter(f)
h2.setLevel(logging.DEBUG)
x.addHandler(h1)
x.addHandler(h2)

class ChordProcessInfo():
    def __init__(self):
        self.localkey = 0
        self.successor = 0
        self.predecessor = 0
        self.fingertable = []
        self.successorlist = []
        self.status = 1

    def set(self,a,b,c,ftab,slist):
        self.localkey = a
        self.successor = b
        self.predecessor = c
        self.fingertable = ftab
        self.successorlist = slist

    def dump(self):
        x.debug("Process {0} status={3} successor {1} predecessor {2}".format(self.localkey,self.successor,self.predecessor,self.status) + "\n\t\tFingertable " + " ".join(str(x) for x in self.fingertable) + "\n\t\tSuccessorlist " + " ".join(str(x) for x in self.successorlist))

ChordProcessDict = {}

def parent( ):
    print('parent')
    pipein = open(pipe_name, 'r')
    while True:
      line = pipein.readline()
      if line[0] == 'S':
          temp = ChordProcessInfo()
          data = line[2:].split(' ')
          temp.localkey  = int(data[0])
          temp.successor = int(data[1])
          temp.predecessor = int(data[2])
          length_table = int(data[3])
          tab1 = []
          for i in range(4,4+length_table):
            tab1.append(int(data[i]))
          tab2 = []
          for i in range(4+length_table,4+2*length_table):
              tab2.append(int(data[i]))
          temp.status = int(data[-1])
          temp.fingertable = tab1
          temp.successorlist = tab2
          temp.dump()
      elif line[0] == 'P':
          print(line[2:])
     
print('monitor_pipe started')
if not os.path.exists(pipe_name):
    os.mkfifo(pipe_name)  
N,keylist = Parser().GetConfig("config.txt")
print(N)
print(keylist)
parent()
